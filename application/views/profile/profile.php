  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        <b>My Yellow</b>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i>My Yellow</a></li>
        <li><a href="#">Home</a></li>
      </ol>
    </section>
 
    <!-- Main content -->
    <section class="content">
 
      <!-- Default box -->
      <div class="box">
        <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Edit Profile</h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            <?php foreach($acc as $acc): ?>
            <form role="form" action="<?php echo site_url('aksesoris/edit_profile') ?>" method="POST" enctype="multipart/form-data">
              <div class="box-body">
                <div class="form-group">
                  <label for="inputBarang1">Nama Lengkap</label>
                  <input type="text" class="form-control" id="inputId1" placeholder="Masukan Nama Lengkap" name="user" value="<?php echo $acc->user ?>">
                <?php echo form_error('aksesoris') ?>
                </div>

                <div class="form-group">
                  <label for="inputBarang1">Alamat Lengkap</label>
                  <input type="text" class="form-control" id="inputBarang1" placeholder="Masukan Alamat Lengkap" name="alamat" value="<?php echo $acc->alamat ?>">
                <?php echo form_error('aksesoris') ?>
                </div>
                
                <div class="form-group">
                  <label for="inputHarga1">Kode Pos</label>
                  <input type="text" class="form-control" id="inputHarga1" placeholder="Masukan Kode Pos" name="kode_pos" value="<?php echo $acc->kode_pos ?>">
                  <?php echo form_error('aksesoris') ?>
                </div>
                
                    <input type="hidden" class="form-control" id="inputId1" placeholder="Masukan Id Barang" name="email" value="<?php echo $acc->email ?>">
                <!-- <div class="checkbox">
                  <label>
                    <input type="checkbox"> Check me out
                  </label>
                </div> -->
              </div>
              <!-- /.box-body -->
              <?php endforeach; ?>
              <div class="box-footer">
                <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#modal-default">Submit</button>
                <?php echo form_error('profile') ?>
              </div>

        <div class="modal fade" id="modal-default">
          <div class="modal-dialog">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Update Profile</h4>
              </div>
              <div class="modal-body">
                <p>Apakah anda yakin akan mengupdate profile? Silahkan login ulang untuk mengupdate data</p>
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary" name="submit">Save changes</button>
              </div>
            </div>
            <!-- /.modal-content -->
          </div>
          <!-- /.modal-dialog -->
        </div>

            </form>
          </div>
      <!-- /.box -->


 
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
 
  <footer class="main-footer">
    <div class="pull-right hidden-xs">
      <b>Version</b> 2.4.0
    </div>
    <strong>Copyright &copy; 2014-2016 <a href="https://adminlte.io">Almsaeed Studio</a>.</strong> All rights
    reserved.
  </footer>