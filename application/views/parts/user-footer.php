<!--   <footer>
  <div class="container">
    
    <div class="copy text-center">
      Copyright 2014 <a href='#'>Website</a>
    </div>
    
  </div>
</footer> -->
<!-- jQuery 3 -->
<script src="<?php echo base_url();?>assets/AdminLTE/bower_components/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap 3.3.7 -->
<script src="<?php echo base_url();?>assets/AdminLTE/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- DataTables -->
<script src="<?php echo base_url();?>assets/AdminLTE/bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
<script src="<?php echo base_url();?>assets/AdminLTE/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
<!-- SlimScroll -->
<script src="<?php echo base_url();?>assets/AdminLTE/bower_components/jquery-slimscroll/jquery.slimscroll.min.js"></script>
<!-- FastClick -->
<script src="<?php echo base_url();?>assets/AdminLTE/bower_components/fastclick/lib/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="<?php echo base_url();?>assets/AdminLTE/dist/js/adminlte.min.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="<?php echo base_url();?>assets/AdminLTE/dist/js/demo.js"></script>
<!-- <script src="https://code.jquery.com/jquery.js"></script> -->
<!-- Include all compiled plugins (below), or include individual files as needed -->
<script src="<?php echo base_url();?>assets/bootstrap/js/jquery-3.3.1.min.js"></script>
<!-- <script src="<?php echo base_url();?>assets/bootstrap/js/bootstrap.js"></script> -->
<script type="text/javascript">
  $(document).ready(function(){
    $('.add_cart').click(function(){
    	var produk_id    = $(this).data("produkid");
		var produk_nama  = $(this).data("produknama");
		var produk_harga = $(this).data("produkharga");
		var quantity     = $('#' + produk_id).val();
      $.ajax({
        url : "<?php echo site_url('aksesoris/add_to_cart') ?>",
        method : "POST",
		data : {produk_id: produk_id, produk_nama: produk_nama, produk_harga: produk_harga, quantity: quantity},
        success: function(data){
          $('#detail_cart').html(data);
          $('#count_cart1').load("<?php echo site_url('aksesoris/count_cart') ?>");
          $('#count_cart2').load("<?php echo site_url('aksesoris/count_cart') ?>");
          $('#list_cart').load("<?php echo site_url('aksesoris/load_list_cart') ?>");
        }
      });
    });

    // Load shopping cart
    $('#detail_cart').load("<?php echo site_url('aksesoris/load_cart') ?>");
    $('#count_cart1').load("<?php echo site_url('aksesoris/count_cart') ?>");
    $('#count_cart2').load("<?php echo site_url('aksesoris/count_cart') ?>");
    $('#list_cart').load("<?php echo site_url('aksesoris/load_list_cart') ?>");


    //Hapus Item Cart
    $(document).on('click','.hapus_cart',function(){
      var row_id=$(this).attr("id"); //mengambil row_id dari artibut id
      $.ajax({
        url : "<?php echo site_url('aksesoris/hapus_cart') ?>",
        method : "POST",
        data : {row_id : row_id},
        success :function(data){
          $('#detail_cart').html(data);
          $('#count_cart1').load("<?php echo site_url('aksesoris/count_cart') ?>");
          $('#count_cart2').load("<?php echo site_url('aksesoris/count_cart') ?>");
          $('#list_cart').load("<?php echo site_url('aksesoris/load_list_cart') ?>");
        }
      });
    });
  });
</script>
</body>
</html>