<!--   <footer>
  <div class="container">
    
    <div class="copy text-center">
      Copyright 2014 <a href='#'>Website</a>
    </div>
    
  </div>
</footer> -->
<!-- jQuery 3 -->
<script src="<?php echo base_url();?>assets/AdminLTE/bower_components/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap 3.3.7 -->
<script src="<?php echo base_url();?>assets/AdminLTE/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- DataTables -->
<script src="<?php echo base_url();?>assets/AdminLTE/bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
<script src="<?php echo base_url();?>assets/AdminLTE/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
<!-- SlimScroll -->
<script src="<?php echo base_url();?>assets/AdminLTE/bower_components/jquery-slimscroll/jquery.slimscroll.min.js"></script>
<!-- FastClick -->
<script src="<?php echo base_url();?>assets/AdminLTE/bower_components/fastclick/lib/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="<?php echo base_url();?>assets/AdminLTE/dist/js/adminlte.min.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="<?php echo base_url();?>assets/AdminLTE/dist/js/demo.js"></script>

<script>
  $(document).ready(function () {
    $('.sidebar-menu').tree();

    // function tampil_orderan(){
    //         $.ajax({
    //             type  : 'ajax',
    //             url   : '<?php echo site_url('aksesoris/list_purchase') ?>',
    //             async : false,
    //             dataType : 'json',
    //             success : function(data){
    //                 var html = '';
    //                 var i;
    //                 foreach (data as $items) {
    //                     html += '<li><a><h4>'+
    //                             '<b>'+$items['email_pembeli']+'</b>'+
    //                             '<small>'+$items['email_pembeli']+'</small>'+
    //                             '</h4><p>'+$items['email_pembeli']+'</p></a></li>'+
    //                             '</tr>';
    //                 }
    //                 $('#list_purchase').html(html);
    //             }
 
    //         });
    //     }
  });
</script>
 <script>
  $(function () {
    $('#example1').DataTable()
    $('#example2').DataTable({
      'paging'      : true,
      'lengthChange': false,
      'searching'   : false,
      'ordering'    : true,
      'info'        : true,
      'autoWidth'   : false
    })
  })

  $('#count_purchase1').load("<?php echo site_url('aksesoris/count_purchase') ?>");
  $('#count_purchase2').load("<?php echo site_url('aksesoris/count_purchase') ?>");
  $('#list_purchase').load("<?php echo site_url('aksesoris/list_purchase') ?>");
</script>
</body>
</html>
