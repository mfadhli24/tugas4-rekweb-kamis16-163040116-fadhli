  <!-- Content Wrapper. Contains page content -->
<div class="container" style="background-color: #f9fafc;">
  <div class="content-wrapper" style="background-color: #f9fafc;">
 
    <!-- Main content -->
    <section class="content">
 
      <!-- Default box -->
      <div class="box box-warning">
        <div class="box-header with-border">
          <h3 class="box-title">Product</h3>
 
          <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
              <i class="fa fa-minus"></i></button>
            <button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
              <i class="fa fa-times"></i></button>
          </div>
        </div>
        <div class="box-body">
          
          <!--  -->
        <div class="row">
          <div class="col-md-12">
            <div class="row">
            <?php foreach($acc as $acc): ?>
              <div class="col-md-3">
                <div class="thumbnail">
                  <img width="200" src="<?php echo base_url().'assets/images/'.$acc->gambar_brg;?>" style="width: 200px; height: 180px;">
                  <div class="caption">
                    <h4><?php echo $acc->nama_brg ?></h4>
                    <div class="row">
                      <div class="col-md-7">
                        <h4><?php echo 'Rp '.number_format($acc->harga_brg);?></h4>
                      </div>
                      <div class="col-md-5">
                        <input type="number" name="quantity" id="<?php echo $acc->id_brg;?>" value="1" class="quantity form-control">
                      </div>
                    </div>
                    <button class="add_cart btn bg-maroon btn-block" data-produkid="<?php echo $acc->id_brg ?>" data-produknama="<?php echo $acc->nama_brg ?>" data-produkharga="<?php echo $acc->harga_brg ?>"> Add To Cart</button>
                  </div>
                </div>
              </div>
            <?php endforeach;?>
            </div>
          </div>
        </div>
          <!--  -->

        </div>
        <!-- /.box-body -->
      </div>
      <!-- /.box -->
 
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
</div>
  