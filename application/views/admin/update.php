  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        <b>My Yellow</b>
        <!-- <small>it all starts here</small> -->
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i>My Yellow</a></li>
        <li><a href="#">Home</a></li>
      </ol>
    </section>
 
    <!-- Main content -->
    <section class="content">
 
      <!-- Default box -->
      <div class="box">
        <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Edit Barang</h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            <form role="form" action="<?php echo site_url('aksesoris/update') ?>" method="POST" enctype="multipart/form-data">
              <div class="box-body">
                  <?php foreach($acc as $acc): ?>
                <div class="form-group">
                  <label for="inputBarang1">Nama Barang</label>
                  <input type="hidden" name="id_brg" value="<?php echo $acc->id_brg ?>">
                  <input type="text" class="form-control" id="inputBarang1" placeholder="Masukan Nama Barang" name="nama_brg" value="<?php echo $acc->nama_brg ?>">
                <?php echo form_error('aksesoris') ?>
                </div>
                
                <div class="form-group">
                  <label for="inputHarga1">Harga Barang</label>
                  <input type="number" class="form-control" id="inputHarga1" placeholder="Masukan Harga Barang" name="harga_brg" value="<?php echo $acc->harga_brg ?>">
                  <?php echo form_error('aksesoris') ?>
                </div>
                
                <div class="row">
                <div class="col-xs-2">
                  <div class="form-group">
                    <label for="inputStok1">Jumlah Barang</label>
                    <input type="number" class="form-control" id="inputStok1" placeholder="Masukan Stok" name="jumlah_brg" value="<?php echo $acc->stok_brg ?>">
                    <?php echo form_error('aksesoris') ?>
                  </div>
                  <!-- /input-group -->
                </div>
                </div>

                <div class="form-group">
                  <label for="inputKategori1">Kategori</label>
                  <input type="text" class="form-control" id="inputKategori1" placeholder="Masukan Kategori" name="kategori" value="<?php echo $acc->kategori ?>">
                  <?php echo form_error('aksesoris') ?>
                </div>

                <div class="form-group">
                  <label for="exampleInputFile">File input</label>
                  <input type="hidden" name="gambar" value="<?php echo $acc->gambar_brg ?>">
                  <input type="file" id="exampleInputFile" name="gambar_brg">
                  <?php echo form_error('aksesoris') ?>
                  <p class="help-block">Example block-level help text here.</p>
                </div>
                <div class="checkbox">
                  <label>
                    <input type="checkbox"> Check me out
                  </label>
                </div>
              <?php endforeach; ?>
              </div>
              <!-- /.box-body -->

              <div class="box-footer">
                <button type="submit" class="btn btn-primary" name="submit">Submit</button>
              </div>
            </form>
          </div>
      <!-- /.box -->
 
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
 
  <footer class="main-footer">
    <div class="pull-right hidden-xs">
      <b>Version</b> 2.4.0
    </div>
    <strong>Copyright &copy; 2014-2016 <a href="https://adminlte.io">Almsaeed Studio</a>.</strong> All rights
    reserved.
  </footer>
 
 