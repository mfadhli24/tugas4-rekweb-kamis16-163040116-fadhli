  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        <b>My Yellow</b>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i>My Yellow</a></li>
        <li><a href="#">Home</a></li>
      </ol>
    </section>
 
    <!-- Main content -->
    <section class="content">
     <div class="row">
      <div class="col-xs-12">
        <div class="box">
            <div class="box-header">
              <h3 class="box-title">Table Barang</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <table id="example2" class="table table-bordered table-hover">
                <thead>
                <tr>
                  <th>No</th>
                  <th>Nama Barang</th>
                  <th>Harga</th>
                  <th>Stok Barang</th>
                  <th>Kategori</th>
                  <th>Aksi</th>
                </tr>
                </thead>
                <tbody>
                <?php $no = 1 ?>
                  <?php foreach($acc as $acc): ?>
                    <tr>
                      <td><?php echo $no++ ?></td>
                      <td><a href="<?php echo site_url('aksesoris/edit/'.$acc->id_brg); ?>"><?php echo $acc->nama_brg ?></a></td>
                      <td><?php echo $acc->harga_brg ?></td>
                      <td><?php echo $acc->stok_brg ?></td>
                      <td><?php echo $acc->kategori ?></td>
                      <td><a href="<?php echo site_url('aksesoris/delete/'.$acc->id_brg); ?>" class="btn btn-block btn-danger btn-sm">Hapus</a></td>
                    </tr>
                  <?php endforeach ?>
                </tbody>
                <tfoot>
                <tr>
                  <th>No</th>
                  <th>Nama Barang</th>
                  <th>Harga</th>
                  <th>Stok Barang</th>
                  <th>Kategori</th>
                  <th>Aksi</th>
                </tr>
                </tfoot>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
      </div>
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
 
  <footer class="main-footer">
    <div class="pull-right hidden-xs">
      <b>Version</b> 2.4.0
    </div>
    <strong>Copyright &copy; 2014-2016 <a href="https://adminlte.io">Almsaeed Studio</a>.</strong> All rights
    reserved.
  </footer>