<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Aksesoris extends CI_Controller {

	public function __construct(){
		parent::__construct();
		$this->load->library('session');
		$this->load->library('cart');
		$this->API = "http://localhost/Tugas3-Rekweb-Kamis16-163040116-Fadhli";
	}

	// --------------- Main View -------------------------------------------------------------------

	public function index()
	{
		$data['acc'] = json_decode($this->curl->simple_get($this->API .'/Aksesoris/'));
		$data['content'] = 'index';
		$this->load->view('templates/guest', $data);
	}

	public function admin()
	{	
		if (!$this->session->userdata('email') == "" && !$this->session->userdata('password') == "" && $this->session->userdata('hak_akses') == "admin") {
			$data['acc'] = json_decode($this->curl->simple_get($this->API .'/Aksesoris/'));
			$data['content'] = 'admin/index';
			$this->load->view('templates/admin', $data);
		} else {
			$this->index();
		}
	}

	public function user()
	{	
		if (!$this->session->userdata('email') == "" && !$this->session->userdata('password') == "" && $this->session->userdata('hak_akses') == "user") {
			$data['acc'] = json_decode($this->curl->simple_get($this->API .'/Aksesoris/'));
			$data['content'] = 'user/index';
			$this->load->view('templates/user', $data);
		} else {
			$this->index();
		}
	}

	// --------------- Main View ---------------------------------------------------------------------------

	// --------------- Proses Login & Logout ---------------------------------------------------------------

	public function login()
	{
		$data['content'] = 'login/login';
		$this->load->view('login/login');
	}

	public function logout()
	{
		$this->session->sess_destroy();
		$this->index();
		// $data['content'] = 'index';
		// $this->load->view('templates/user', $data);
	}

	public function signin(){
		if (isset($_POST['submit'])) {
			$this->cart->destroy();
			$email = $this->input->post('email');
			$password = $this->input->post('password');
			// $where = array(
			// 	'email' => $email,
			// 	'password' => md5($password)
			// );
			$cek = json_decode($this->curl->simple_get($this->API .'/pengguna/', array("email" => $email,"password" => md5($password))));
			if ($cek != null) {
				// $cek = $this->db->affected_rows();
				$data_session = array(
				'user' => $cek[0]->user,
				'email' => $email,
				'password' => md5($password),
				'hak_akses' => $cek[0]->hak_akses
				);
				$this->session->set_userdata($data_session);
				if ($cek[0]->hak_akses == "admin") {
						$this->admin();
					} else {
						$this->user();
				}
			} else {
				$this->session->set_flashdata('login', '<small><p class="text-danger">Email atau password Salah.</p></small>');	
				$this->login();
			}
		}
	}

	// --------------- Proses Login & Logout ------------------------------------------------------------

	// --------------- Proses Registrasi ----------------------------------------------------------------

	public function register()
	{
		$this->load->view('register/register');
	}

	public function add_register()
	{
		$this->_rules_register();

		$user = $this->input->post('user');
		$email = $this->input->post('email');
		$password1 = $this->input->post('password1');
		$password2 = $this->input->post('password2');
		$check = $this->input->post('check');

		if($this->form_validation->run() == FALSE){
			$this->register();
		} else {
			if ($password1 == $password2) {
			$cek = json_decode($this->curl->simple_get($this->API .'/pengguna/', array("email" => $email)));
				if ($cek <= 0) {
					$data = array(
					'user' => $user,
					'email' => $email,
					'password' => md5($password1),
					'hak_akses' => 'user'
					);

					$profile = array(
					'user' => $user,
					'email' => $email,
					'alamat' => '',
					'kode_pos' => ''
					);
				$this->curl->simple_post($this->API . '/pengguna/', $data, array(CURLOPT_BUFFERSIZE => 10));
		        $this->curl->simple_post($this->API . '/data_pengguna/', $profile, array(CURLOPT_BUFFERSIZE => 10));
				$this->login();
				} else {
					$this->register();
					$this->session->set_flashdata('email', '<small><p class="text-danger">Email Sudah Terdaftar</p></small>');	
				}
			} else {
				$this->session->set_flashdata('password', '<small><p class="text-danger">Password Tidak Cocok</p></small>');
				$this->register();
			}
			
		}
	}

	// --------------- Proses Registrasi -----------------------------------------------------------------

	// --------------- Proses CRUD -----------------------------------------------------------------------

	public function insert()
	{
		if (!$this->session->userdata('email') == "" && !$this->session->userdata('password') == "" && $this->session->userdata('hak_akses') == "admin") {
			$data['acc'] = json_decode($this->curl->simple_get($this->API .'/Aksesoris/'));
			$data['content'] = 'admin/insert';
			$this->load->view('templates/admin', $data);
		} else {
			$this->index();
		}
	}

	public function add() 
	{

		$this->_rules()	;
		$id_brg = $this->input->post('id_brg');
		$nama_brg = $this->input->post('nama_brg');
		$harga_brg = $this->input->post('harga_brg');
		$jumlah_brg = $this->input->post('jumlah_brg');
		$kategori = $this->input->post('kategori');
		$gambar = $this->_uploadImage();
		$this->load->model('Aksesoris_model');
		if($this->form_validation->run() == FALSE){
			$this->insert();
		} else {
			$data = array(
				'id_brg' => $id_brg,
				'nama_brg' => $nama_brg,
				'harga_brg' => $harga_brg,
				'stok_brg' => $jumlah_brg,
				'kategori' => $kategori,
				'gambar_brg' => $gambar,
			);
			$this->curl->simple_post($this->API . '/aksesoris/', $data, array(CURLOPT_BUFFERSIZE => 10));
			$this->admin();
		}
	}

	public function kategori($kategori)
	{
		if (!$this->session->userdata('email') == "" && !$this->session->userdata('password') == "" && $this->session->userdata('hak_akses') == "user") {
			$cek = json_decode($this->curl->simple_get($this->API .'/aksesoris/', array("kategori" => $kategori)));
			$data['acc'] = $cek;
			$data['content'] = 'user/index';
			$this->load->view('templates/user', $data);
		} else {
			$cek = json_decode($this->curl->simple_get($this->API .'/aksesoris/', array("kategori" => $kategori)));
			$data['acc'] = $cek;
			$data['content'] = 'index';
			$this->load->view('templates/guest', $data);
		}
	}

	public function delete($id)
	{
		$data['acc'] = json_decode($this->curl->simple_delete($this->API . '/aksesoris/', array('id_brg' =>$id), array(CURLOPT_BUFFERSIZE => 10)));
		$this->admin();
	}

	public function update() 
	{
		if (isset($_POST['submit'])) {
			$id_brg = $this->input->post('id_brg');
			$nama_brg = $this->input->post('nama_brg');
			$harga_brg = $this->input->post('harga_brg');
			$jumlah_brg = $this->input->post('jumlah_brg');
			$kategori = $this->input->post('kategori');
			
			if (!empty($_FILES["gambar_brg"]["name"])) {
				$gambar_brg = $this->_uploadImage();
			} else {
				$gambar_brg = $this->input->post('gambar');
			}
			$data = array(
				'id_brg' => $id_brg,
				'nama_brg' => $nama_brg,
				'harga_brg' => $harga_brg,
				'stok_brg' => $jumlah_brg,
				'kategori' => $kategori,
				'gambar_brg' => $gambar_brg,
			);
			$this->curl->simple_put($this->API . '/aksesoris/', $data, array(CURLOPT_BUFFERSIZE => 10));
			$this->admin();
		}
	}

	public function edit($id)
	{
		if (!$this->session->userdata('email') == "" && !$this->session->userdata('password') == "" && $this->session->userdata('hak_akses') == "admin") {
			$data['acc'] = json_decode($this->curl->simple_get($this->API . '/aksesoris/', array('id_brg' =>$id), array(CURLOPT_BUFFERSIZE => 10)));
			$data['content'] = 'admin/update';
			$this->load->view('templates/admin', $data);
		} else {
			$this->index();
		}
	}

	// --------------- Proses CRUD ----------------------------------------------------------------------

	// --------------- Profile --------------------------------------------------------------------------

	public function profile()
	{
		if (!$this->session->userdata('email') == "" && !$this->session->userdata('password') == "" && $this->session->userdata('hak_akses') == "admin") {
	
			$email = $this->session->userdata('email');
			$password = $this->session->userdata('password');
			$data['acc'] = json_decode($this->curl->simple_get($this->API .'/data_pengguna/', array("email" => $email)));
			$data['content'] = 'profile/profile';
			$this->load->view('templates/admin', $data);
		} else {
			$email = $this->session->userdata('email');
			$password = $this->session->userdata('password');
			$data['acc'] = json_decode($this->curl->simple_get($this->API .'/data_pengguna/', array("email" => $email)));
			$data['content'] = 'profile/profile';
			$this->load->view('templates/user', $data);
		}
	}

	public function edit_profile(){
		if (!$this->session->userdata('email') == "" && !$this->session->userdata('password') == "" && $this->session->userdata('hak_akses') == "admin") {
			if (isset($_POST['submit'])) {
			$user = $this->input->post('user');
			$email = $this->input->post('email');
			$alamat = $this->input->post('alamat');
			$kode_pos = $this->input->post('kode_pos');
				$data = array(
					'user' => $user,
					'alamat' => $alamat,
					'kode_pos' => $kode_pos,
					'email' => $email,
				);

				$user = array(
					'user' => $user,
					'email' => $email,
				);

				$this->session->set_flashdata('profile', '<small><p class="text-Success">Profile berhasil di update.</p></small>');
				$this->curl->simple_put($this->API . '/pengguna/', $user, array(CURLOPT_BUFFERSIZE => 10));
		        $this->curl->simple_put($this->API . '/data_pengguna/', $data, array(CURLOPT_BUFFERSIZE => 10));
				$this->admin();
			}
		} else {
			if (isset($_POST['submit'])) {
			$user = $this->input->post('user');
			$email = $this->input->post('email');
			$alamat = $this->input->post('alamat');
			$kode_pos = $this->input->post('kode_pos');
				$data = array(
					'user' => $user,
					'alamat' => $alamat,
					'kode_pos' => $kode_pos,
					'email' => $email,
				);

				$user = array(
					'user' => $user,
					'email' => $email,
				);

				$this->session->set_flashdata('profile', '<small><p class="text-Success">Profile berhasil di update.</p></small>');
				$this->curl->simple_put($this->API . '/pengguna/', $user, array(CURLOPT_BUFFERSIZE => 10));
		        $this->curl->simple_put($this->API . '/data_pengguna/', $data, array(CURLOPT_BUFFERSIZE => 10));
				$this->logout();
			}
		}
	}

	// --------------- Profile -----------------------------------------------------------------------

	// --------------- Cart --------------------------------------------------------------------------

	public function add_to_cart(){ //fungsi Add To Cart
		// $this->load->model('Aksesoris_model');
		// $cek = $this->Aksesoris_model->cek_signin($where);
		// $row = $cek->row_array();
		$acc = array(
				'id' => $this->input->post('produk_id'), 
				'name' => $this->input->post('produk_nama'), 
				'price' => $this->input->post('produk_harga'), 
				'qty' => $this->input->post('quantity'), 
			);
		$this->cart->insert($acc);
		echo $this->show_cart(); //tampilkan cart setelah added
		echo $this->list_cart(); //tampilkan cart setelah added
	}

	public function show_cart(){ //Fungsi untuk menampilkan Cart
		$output = '';
		$no = 0;
		foreach ($this->cart->contents() as $items) {
			$no++;
			$output .='
				<tr>
					<td>'.$items['name'].'</td>
					<td>'.number_format($items['price']).'</td>
					<td>'.$items['qty'].'</td>
					<td>'.number_format($items['subtotal']).'</td>
					<td><button type="button" id="'.$items['rowid'].'" class="hapus_cart btn btn-danger btn-xs">Hapus</button></td>
				</tr>
			';
		}
		$output .= '
			<tr>
				<th colspan="3">Total</th>
				<th colspan="2">'.'Rp '.number_format($this->cart->total()).'</th>
			</tr>
			<tr>
				<td colspan="5"><center><button type="submit" id="bayar" class="reset-cart btn bg-maroon">Bayar</button></center></td>
			</tr>
		';
		return $output;
	}

	public function list_cart(){ //Fungsi untuk menampilkan Cart
		$output = '';
		$no = 0;
		foreach ($this->cart->contents() as $items) {
			$no++;
			$output .='
				<li>
                    <a>            			
                      <h4>
                        <b>'.$items['name'].'</b>
                        <small><button type="button" id="'.$items['rowid'].'" class="hapus_cart btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
              			<i class="fa fa-times"></i></button></small>
                      </h4>
                      <p>'.$items['qty'].'</p>
                    </a>
                </li>
			';
		}
		return $output;
	}

	public function detail_cart(){ //Fungsi untuk menampilkan Cart
		if (!$this->session->userdata('email') == "" && !$this->session->userdata('password') == "" && $this->session->userdata('hak_akses') == "user") {
			$this->load->model('Aksesoris_model');
			$data['acc'] = json_decode($this->curl->simple_get($this->API .'/Aksesoris/'));
			$data['content'] = 'user/detail_cart';
			$this->load->view('templates/user', $data);
		} else {
			$this->load->model('Aksesoris_model');
			$data['acc'] = json_decode($this->curl->simple_get($this->API .'/Aksesoris/'));
			$data['content'] = 'detail_cart';
			$this->load->view('templates/guest', $data);
		}
	}

	public function count_cart(){
		$no = 0;
		foreach ($this->cart->contents() as $items){
		$no++;
		}
		echo $no;

		return $no;
	}

	public function load_cart(){ //load data cart
		echo $this->show_cart();
	}
	public function load_list_cart(){ //load data cart
		echo $this->list_cart();
	}

	public function hapus_cart(){ //fungsi untuk menghapus item cart
		$data = array(
			'rowid' => $this->input->post('row_id'), 
			'qty' => 0, 
		);
		$this->cart->update($data);
		echo $this->show_cart();
		echo $this->list_cart();
	}

	public function reset_cart(){
		foreach ($this->cart->contents() as $items) {
			$data = array(
				'rowid' => $this->input->post('row_id'), 
				'qty' => 0, 
			);

			$this->cart->update($data);
		}
			$this->cart->update($data);	
	}

	// --------------- Cart ---------------------------------------------------------------------------

	// --------------- Purchase / Pembelian -----------------------------------------------------------

	public function purchase()
	{	
		if (!$this->session->userdata('email') == "" && !$this->session->userdata('password') == "" && $this->session->userdata('hak_akses') == "admin") {
			$this->load->model('Aksesoris_model');
			$cek = json_decode($this->curl->simple_get($this->API .'/pembelian/'));
			$data['acc'] = $cek;
			$data['content'] = 'admin/purchase';
			$this->load->view('templates/admin', $data);
		} else {
			$this->index();
		}
	}

	public function purchase_now()
	{	
		if (!$this->session->userdata('email') == "" && !$this->session->userdata('password') == "" && $this->session->userdata('hak_akses') == "admin") {
			$this->load->model('Aksesoris_model');
			$where = array('tanggal' => date('Y-m-d'));
			$cek = json_decode($this->curl->simple_get($this->API .'/pembelian/', array('tanggal' => date('Y-m-d'))));
			$data['acc'] = $cek;
			$data['content'] = 'admin/purchase';
			$this->load->view('templates/admin', $data);
		} else {
			$this->index();
		}
	}

	public function count_purchase(){
		$this->load->model('Aksesoris_model');
		$where = array('tanggal' => date('Y-m-d'));
		$cek = json_decode($this->curl->simple_get($this->API .'/pembelian/', array('tanggal' => date('Y-m-d'))));
		

		echo count($cek);
		return $cek;
	}
	public function list_purchase(){
		$no = 0;
		$this->load->model('Aksesoris_model');
		$where = array('tanggal' => date('Y-m-d'));
		$cek = json_decode($this->curl->simple_get($this->API .'/pembelian/', array('tanggal' => date('Y-m-d'))));
		$data = $cek;
		$row = json_encode($data);
		$results = json_decode($row, true);
		
		$output = '';

		foreach ($results as $items) {
			$no++;
		echo $output ='
				<li>
                    <a>            			
                      <h4>
                        <b>'. $items["email_pembeli"] .'</b>
                        <small>'. $items["jumlah"].'</small>
                      </h4>
                      <p>'.$items["nama_brg"].'</p>
                    </a>
                </li>

			';
		}
		return $output;
	}

	public function pembelian()
	{
		if (!$this->session->userdata('email') == "" && !$this->session->userdata('password') == "" && $this->session->userdata('hak_akses') == "user") {

			foreach ($this->cart->contents() as $items) {
			$id_brg = $items['id'];
			$nama_brg = $items['name'];
			$total_harga = $items['subtotal'];
			$jumlah = $items['qty'];
			$email = $this->session->userdata('email');
	
				$data = array(
					'id_brg' => $id_brg,
					'email_pembeli' => $email,
					'nama_brg' => $nama_brg,
					'jumlah' => $jumlah,
					'total_harga' => $total_harga,
					'tanggal' => date('Y-m-d')
				);
				$this->curl->simple_post($this->API . '/pembelian/', $data, array(CURLOPT_BUFFERSIZE => 10));
				$data['acc'] = json_decode($this->curl->simple_get($this->API .'/aksesoris/'));
			}
				
				$this->user();
				$this->cart->destroy();

		} else {
			$this->cart->destroy();
			$this->login();
		}
	}

	// --------------- Purchase / Pembelian ---------------------------------------------------

	// --------------- Validation -------------------------------------------------------------	

	public function _rules()
	{
		$this->form_validation->set_rules('nama_brg', 'nama barang', 'trim|required')	;
		$this->form_validation->set_rules('harga_brg', 'harga barang', 'trim|required')	;
		$this->form_validation->set_rules('jumlah_brg', 'jumlah barang', 'trim|required')	;
		$this->form_validation->set_rules('kategori', 'kategori', 'trim|required')	;
		// $this->form_validation->set_rules('gambar', 'gambar', 'trim|required')	;
		$this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>')	;
	}

	public function _rules_register()
	{
		$this->form_validation->set_rules('user', 'user', 'trim|required')	;
		$this->form_validation->set_rules('email', 'email', 'trim|required')	;
		$this->form_validation->set_rules('password1', 'password', 'trim|required')	;
		$this->form_validation->set_rules('password2', 'Retype password', 'trim|required')	;
		$this->form_validation->set_rules('check', 'Checked', 'trim|required')	;
		$this->form_validation->set_error_delimiters('<small><p class="text-danger">', '</p></small>')	;
	}

	// --------------- Validation -----------------------------------------------------------

	// --------------- Other ----------------------------------------------------------------

	private function _uploadImage()
	{
	    $config['upload_path']          = 'D:\xampp\htdocs\Tugas2-Rekweb-Kamis16-163040116-Fadhli\assets\images';
	    $config['allowed_types']        = 'gif|jpg|png';
	    $config['file_name']            = $this->input->post('id_brg');
	    $config['overwrite']			= true;
	    $config['max_size']             = 1024; // 1MB
	    // $config['max_width']            = 1024;
	    // $config['max_height']           = 768;
	    $this->load->library('upload', $config);
	    if ($this->upload->do_upload('gambar_brg')) {
	        return $this->upload->data("file_name");
	    }
	    
	    return "default.jpg";
	}

}
